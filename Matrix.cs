using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;


namespace Matrix
{
    class Program
    {
        static void Main(string[] args)
        {
            //ConsoleColor[] colors = { ConsoleColor.Red, ConsoleColor.Yellow, ConsoleColor.Green, ConsoleColor.Blue, ConsoleColor.DarkBlue, ConsoleColor.Magenta };
            ConsoleColor[] colors = { ConsoleColor.Blue, ConsoleColor.Green };

            int atColorIndex = 0;
            Random rand = new Random();
            String str = "";

            Console.Write("Press ENTER to start...");
            Console.ReadKey();


            for (int i = 0; i < 100; i++)
            {

                if (i % 2 == 0)
                {
                    str = "";
                    for (int j = 0; j < 30; j++)
                    {
                        Thread.Sleep(1);

                        Console.ForegroundColor = colors[atColorIndex];
                        int n = rand.Next(5);

                        if (n < 2) { 
                            str += n.ToString(); 
                        } else { 
                            str += " "; 
                        }

                        atColorIndex++;

                        if (atColorIndex == colors.Length) { atColorIndex = 0; }

                        if(str.Length >= 10)
                        {
                            Console.Write(str);
                        }
                    }                    
                }
            }
            Console.WriteLine("End of screen...");
            Console.Write("Press any key to exit...");
            Console.ReadKey();
        }
    }
}